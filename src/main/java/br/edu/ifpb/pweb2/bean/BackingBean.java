package br.edu.ifpb.pweb2.bean;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;

import br.edu.ifpb.pweb2.model.EstadoCivilEnum;

@Named(value="bbean")
@RequestScoped
public class BackingBean {
	
	private String mensagem = "Produzido no bbean";
	private String nome;
	private Integer idade;
	private String faixa;
	private String senha;
	private Integer cidade;
	private String[] cidades = {"Jo�o Pessoa", "Campina Grande", "Tapero�", "Soledade", "Juazeirinho"};
	private String nomeCidade;
	private EstadoCivilEnum estCivil;
	
		public EstadoCivilEnum[] getEstadosCivis() {
		return EstadoCivilEnum.values();
	}
	
	public void selecioneEstCivil(ActionEvent e) {
		// vazio!
	}
	
	public void selecioneCidade(ActionEvent e) {
		if(cidade != null) {
			this.nomeCidade = cidades[cidade];
		}
	}
	
	public String valideSenha() {
		FacesContext fc	= FacesContext.getCurrentInstance();
		String msg = "Senha n�o confere!";
		FacesMessage.Severity nivel	= FacesMessage.SEVERITY_ERROR;
		if (this.senha.equalsIgnoreCase("secret")) {
			msg	= "Senha confere!";
			nivel = FacesMessage.SEVERITY_INFO;
		}
		FacesMessage facesMsg = new FacesMessage(nivel, msg, null);
		fc.addMessage("form4:senha", facesMsg);
		return null;
	}
	
	public String calculeFaixa() {
		
		if(this.idade > 17 && this.idade < 26) {
			this.faixa = "[Jovem]";
		} else if (this.idade > 25 && this.idade < 60) {
			this.faixa = "[Adulto]";
		} else if (this.idade > 59 && this.idade < 90) {
			this.faixa = "[Idoso]";
		}
		
		return null;
	}
	
	public String maiusculas() {
		this.nome = this.nome.toUpperCase();
		return null;
	}
	
	public String minusculas() {
		this.nome = this.nome.toLowerCase();
		return null;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public String getMensagem() {
		return this.mensagem;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getFaixa() {
		return faixa;
	}

	public void setFaixa(String faixa) {
		this.faixa = faixa;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Integer getCidade() {
		return cidade;
	}

	public void setCidade(Integer cidade) {
		this.cidade = cidade;
	}

	public String[] getCidades() {
		return cidades;
	}

	public void setCidades(String[] cidades) {
		this.cidades = cidades;
	}

	public String getNomeCidade() {
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}
	
	public EstadoCivilEnum getEstCivil() {
		return estCivil;
	}

	public void setEstCivil(EstadoCivilEnum estCivil) {
		this.estCivil = estCivil;
	}

}

